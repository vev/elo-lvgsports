# framagit.org/vev
import tkinter as tk
from tkinter import ttk
import json
from csv import writer
from time import localtime, strftime
import csv
import random

"""RAF
* Scrollbar
* comment/struct prog
* seq bouton radio
* controle: si pas j1ab2ab pas bouton g/p/v: comment rendre invisible
* 😊❤😍😂🤣💖😜🐱‍🚀💋

* 5 derniers matchs
    pb index qd moins de 11 lignes
    log match cr/lf en trop
* config simple XOR double

"""

# _______________
# _______________
# * import json *
# _______________
# _______________
with open('config.json', 'r') as f:
    config = json.load(f)
with open('joueurs.json', 'r') as f:
    joueurs = json.load(f)
with open('equipes_double.json', 'r') as f:
    equipes_double = json.load(f)

# liste_joueurs depuis json
liste_joueurs = list(joueurs.keys())

# ______________________________
# ______________________________
#   * prépa liste classement *
# ______________________________
# ______________________________
# ____classement_joueur___
classement_joueur ={}
for c in joueurs:     # __création dico 'classement_joueur' nom:elo__
    classement_joueur[c] = joueurs[c][0]# copie valeur dans clé
classement_joueur = {k: v for k, v in sorted(classement_joueur.items(), key=lambda item: item[1],reverse=True)} # tri décroissant par valeurs
liste_nom_joueur_top= list(classement_joueur)# extrait clés
# liste suivante pas utile car on a déjà les clés triées qu'on peut utiliser pour chercher infos dans dico
liste_elo_joueur_top= list(classement_joueur.values())# extrait valeurs

# ____classement_double____
classement_double ={}
for c in equipes_double:     # __création dico 'classement_double' nom:elo__
    classement_double[c] = equipes_double[c][0]# copie valeur dans clé
classement_double = {k: v for k, v in sorted(classement_double.items(), key=lambda item: item[1],reverse=True)} # tri décroissant par valeurs
liste_nom_double_top= list(classement_double)# extrait clés
# liste suivante pas utile car on a déjà les clés triées qu'on peut utiliser pour chercher infos dans dico
liste_elo_double_top= list(classement_double.values())# extrait valeurs

# _______________
# _______________
# ___ config ____
# _______________
# _______________

nb_joueurs = len(joueurs)
nb_doubles = len(equipes_double)
ID_dernier_match = 0
var_message_bienvenue= config["message_bienvenue"][0] # message_bienvenue


# _______________
# _______________
# __ Fonctions __
# _______________
# _______________

def recup_gagnant(*args): 
	"""recup_gagnant le nom_joueurs et elo"""
	var_j1a= j1a.get()
	var_j1b= j1b.get()
	if liste_joueurs.index(var_j1a) < liste_joueurs.index(var_j1b): # ordre noms pour equipe double
		var_j1= var_j1a + "_" + var_j1b
	if liste_joueurs.index(var_j1b) < liste_joueurs.index(var_j1a): # ordre noms pour equipe double
		var_j1= var_j1b + "_" + var_j1a
	# création equipes_double si pas existant
	if var_j1 not in equipes_double:
		d= {var_j1:["1500", "0", "0", "0", "aucun enregistrement"],}
		equipes_double.update(d)
		with open('equipes_double.json', 'w') as f:
			json.dump(equipes_double, f, ensure_ascii=False)
	# controle
	if var_j1a != var_j1b:
		j1.set(var_j1)
		j1_elo.set(equipes_double[var_j1][0])
		j1_mj.set(equipes_double[var_j1][1])
		j1_mg.set(equipes_double[var_j1][2])
		j1_mp.set(equipes_double[var_j1][3])
		j1_dt.set(equipes_double[var_j1][4])

def recup_perdant(*args): 
	"""recup_perdant le nom_joueurs et elo
	
	         proba
	"""
	var_j1a= j1a.get()
	var_j1b= j1b.get()
	var_j2a= j2a.get()
	var_j2b= j2b.get()
	if liste_joueurs.index(var_j2a) < liste_joueurs.index(var_j2b): #ordre noms pour equipe double
		var_j2= var_j2a + "_" + var_j2b
	if liste_joueurs.index(var_j2b) < liste_joueurs.index(var_j2a): #ordre noms pour equipe double
		var_j2= var_j2b + "_" + var_j2a
	# création equipes_double si pas existant
	if var_j2 not in equipes_double:
		d= {var_j2:["1500", "0", "0", "0", "aucun enregistrement"],}
		equipes_double.update(d)
		with open('equipes_double.json', 'w') as f:
			json.dump(equipes_double, f, ensure_ascii=False)
	# controle
	if var_j1a != var_j1b != var_j2a != var_j2b: 
		j2.set(var_j2)
		j2_elo.set(equipes_double[var_j2][0])
		j2_mj.set(equipes_double[var_j2][1])
		j2_mg.set(equipes_double[var_j2][2])
		j2_mp.set(equipes_double[var_j2][3])
		j2_dt.set(equipes_double[var_j2][4])
		# _________code1 calcul proba________________
		var_j1_elo= int(j1_elo.get())
		var_j2_elo= int(j2_elo.get())
		var_proba= 1 / (1 + 10 ** ((var_j2_elo-var_j1_elo)/400))
		j1_proba.set(round(var_proba,2))
		j2_proba.set(round(1-var_proba,2))
		# _________________________



def valider(*args): 
	"""calcule proba et nouveau elo
	maj fichiers
	"""
	# _________code1 calcul proba________________
	var_j1_elo= int(j1_elo.get())
	var_j2_elo= int(j2_elo.get())
	var_proba= 1 / (1 + 10 ** ((var_j2_elo-var_j1_elo)/400))
	j1_proba.set(round(var_proba,2))
	j2_proba.set(round(1-var_proba,2))
	# ___________calcul ELO______________
	j1_n_elo.set(round(var_j1_elo+(1-var_proba)*32))
	j2_n_elo.set(round(var_j2_elo-(1-var_proba)*32))
	# ___________double gagnant______________
	var_j1= j1.get()
	var_j1_n_elo= j1_n_elo.get()
	var_j1_mj= int(equipes_double[var_j1][1]) # matchs joués
	var_j1_mg= int(equipes_double[var_j1][2]) # matchs gagnés
	var_j1_mp= int(equipes_double[var_j1][3]) # matchs perdus
	# ___________double perdant______________
	var_j2= j2.get()
	var_j2_n_elo= j2_n_elo.get()
	var_j2_mj= int(equipes_double[var_j2][1]) # matchs joués
	var_j2_mg= int(equipes_double[var_j2][2]) # matchs gagnés
	var_j2_mp= int(equipes_double[var_j2][3]) # matchs perdus
	# ______j1a gagnant_______
	var_j1a= j1a.get()
	var_j1a_mj= int(joueurs[var_j1a][1]) # matchs joués
	var_j1a_mg= int(joueurs[var_j1a][2]) # matchs gagnés
	var_j1a_mp= int(joueurs[var_j1a][3]) # matchs perdus
	var_j1a_n_elo= round((var_j1_n_elo + (int(joueurs[var_j1a][0]) * var_j1a_mj)) / (var_j1a_mj +1)) # n_elo = (elo double + elo joueur*mj)/mj+1
	# ______j1b gagnant_______
	var_j1b= j1b.get()
	var_j1b_mj= int(joueurs[var_j1b][1]) # matchs joués
	var_j1b_mg= int(joueurs[var_j1b][2]) # matchs gagnés
	var_j1b_mp= int(joueurs[var_j1b][3]) # matchs perdus
	var_j1b_n_elo= round((var_j1_n_elo + (int(joueurs[var_j1b][0]) * var_j1b_mj)) / (var_j1b_mj +1)) # n_elo = (elo double + elo joueur*mj)/mj+1
	# ______j2a perdant_______
	var_j2a= j2a.get()
	var_j2a_mj= int(joueurs[var_j2a][1]) # matchs joués
	var_j2a_mg= int(joueurs[var_j2a][2]) # matchs gagnés
	var_j2a_mp= int(joueurs[var_j2a][3]) # matchs perdus
	var_j2a_n_elo= round((var_j2_n_elo + (int(joueurs[var_j2a][0]) * var_j2a_mj)) / (var_j2a_mj +1)) # n_elo = (elo double + elo joueur*mj)/mj+1
	# ______j2b perdant_______
	var_j2b= j2b.get()
	var_j2b_mj= int(joueurs[var_j2b][1]) # matchs joués
	var_j2b_mg= int(joueurs[var_j2b][2]) # matchs gagnés
	var_j2b_mp= int(joueurs[var_j2b][3]) # matchs perdus
	var_j2b_n_elo= round((var_j2_n_elo + (int(joueurs[var_j2b][0]) * var_j2b_mj)) / (var_j2b_mj +1)) # n_elo = (elo double + elo joueur*mj)/mj+1
	# __________MAJ JSON et CSV_______________
	dt = strftime("%Y-%m-%d %H:%M:%S", localtime()) # date/heure dernier match joué
	d= {var_j1:[str(var_j1_n_elo),str(var_j1_mj+1),str(var_j1_mg+1),str(var_j1_mp),str(dt)],var_j2:[str(var_j2_n_elo),str(var_j2_mj+1),str(var_j2_mg),str(var_j2_mp+1),str(dt)]}
	equipes_double.update(d)
	with open('equipes_double.json', 'w') as f:
		json.dump(equipes_double, f, ensure_ascii=False)
	e= {var_j1a:[str(var_j1a_n_elo),str(var_j1a_mj+1),str(var_j1a_mg+1),str(var_j1a_mp),str(dt)],var_j1b:[str(var_j1b_n_elo),str(var_j1b_mj+1),str(var_j1b_mg+1),str(var_j1b_mp),str(dt)],var_j2a:[str(var_j2a_n_elo),str(var_j2a_mj+1),str(var_j2a_mg),str(var_j2a_mp+1),str(dt)],var_j2b:[str(var_j2b_n_elo),str(var_j2b_mj+1),str(var_j2b_mg),str(var_j2b_mp+1),str(dt)]}
	joueurs.update(e)
	with open('joueurs.json', 'w') as f:
		json.dump(joueurs, f, ensure_ascii=False)
	var_ID_dernier_match= int(config["ID_dernier_match"][0]) # numéro_dernier_match
	g= {"ID_dernier_match": [str(var_ID_dernier_match +1)]}
	config.update(g)
	with open('config.json', 'w') as f:
		json.dump(config, f, ensure_ascii=False)
	# __________RAZ var_______________
	"""j1a.set("")
	j1b.set("")
	j2a.set("")_____________ pour test
	j2b.set("")"""
	# __________log match_______________
	var_commentaire = commentaire.get()
	résultat_match = [str(var_ID_dernier_match +1),str(var_j1),str(var_j1_elo),str(round(var_proba,2)),str(var_j2),str(var_j2_elo),str(round(1-var_proba,2)),str(var_j1_n_elo),str(var_j2_n_elo),str(dt),str(var_commentaire)]
	with open(r'matchs.csv', 'a', newline='') as f:
		writer_object = writer(f)
		writer_object.writerow(résultat_match)
		f.close()



# _________________________
# _________________________
# __ Interface graphique __
# _________________________
# _________________________

# fenetre principale
fenetre = tk.Tk()
fenetre.title("elo LVGsports double")

# élément (widget) cadre contenant tout le reste
cadre = ttk.Frame(fenetre, padding="3 3 12 12")
cadre.grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E, tk.S))
cadre.pack(side ='top')

# le cadre s'étire si l'on étire la fenêtre
fenetre.columnconfigure(0, weight=1)
fenetre.rowconfigure(0, weight=1)
s = ttk.Style()

print(s.theme_names())
a=random.choice(s.theme_names())
print(a)
s.theme_use(a)


# Paramètres du système (variables)
commentaire = tk.StringVar()
j1a = tk.StringVar()
j1b = tk.StringVar()
j2a = tk.StringVar()
j2b = tk.StringVar()
j1 = tk.StringVar()
j2 = tk.StringVar()
j1_elo = tk.StringVar()
j1_mj = tk.StringVar()
j1_mg = tk.StringVar()
j1_mp = tk.StringVar()
j1_dt = tk.StringVar()
j2_elo = tk.StringVar()
j2_mj = tk.StringVar()
j2_mg = tk.StringVar()
j2_mp = tk.StringVar()
j2_dt = tk.StringVar()
j1_proba = tk.IntVar()
j2_proba = tk.IntVar()
j1_n_elo = tk.IntVar()
j2_n_elo = tk.IntVar()


# Création des quatres 'boutons radio' :
for n in range(nb_joueurs):
	boutj1a = ttk.Radiobutton(cadre, text = liste_joueurs[n], variable = j1a, value = liste_joueurs[n])
	boutj1a.grid(column=1, row = n, sticky=(tk.N, tk.S, tk.W, tk.E))
	boutj1b = ttk.Radiobutton(cadre, text = liste_joueurs[n], variable = j1b, value = liste_joueurs[n]).grid(column=2, row = n, sticky=(tk.N, tk.S, tk.W, tk.E))
	boutj2a = ttk.Radiobutton(cadre, text = liste_joueurs[n], variable = j2a, value = liste_joueurs[n]).grid(column=1, row = n+nb_joueurs+5, sticky=(tk.N, tk.S, tk.W, tk.E))
	boutj2b = ttk.Radiobutton(cadre, text = liste_joueurs[n], variable = j2b, value = liste_joueurs[n]).grid(column=2, row = n+nb_joueurs+5, sticky=(tk.N, tk.S, tk.W, tk.E))


#       virer Radiobutton boutj1a.grid_remove()


# Placement des éléments (widgets)

# Création du bouton
ttk_bouton_gagne = ttk.Button(cadre, text="gagnent 😊", command=recup_gagnant).grid(column=3, row=nb_joueurs+2, sticky=tk.W)
ttk_bouton_perd = ttk.Button(cadre, text="perdent 😂", command=recup_perdant).grid(column=3, row=nb_joueurs+3, sticky=tk.W)
ttk_bouton_valider = ttk.Button(cadre, text="valider", command=valider).grid(column=6, row=nb_joueurs+4, sticky=tk.W)


#noms des joueurs
ttk_nom_joueurs_gagnants = ttk.Label(cadre, textvariable=j1).grid(column=4, row=nb_joueurs+2, sticky=(tk.W, tk.E))
ttk_nom_joueurs_perdants = ttk.Label(cadre, textvariable=j2).grid(column=4, row=nb_joueurs+3, sticky=(tk.W, tk.E))

# Logo et titre
tk_img = tk.PhotoImage(file = r".\v\LogonewLVGB.png")
img1 =tk_img.subsample(3, 3)
ttk_logo = ttk.Label(cadre, image = img1).grid(row = 0, column = 5, columnspan = 2, rowspan = 7, sticky=(tk.W, tk.N))
ttk_qui_quoi = ttk.Label(cadre, text=var_message_bienvenue).grid(column=7, row=3, columnspan = 2, rowspan = 2, sticky=(tk.W, tk.E))


titre_mj = ttk.Label(cadre, text="nb matchs\njoués :").grid(column=5, row=nb_joueurs+1, sticky=tk.W)
mj_joueurs_gagnants = ttk.Label(cadre, textvariable=j1_mj).grid(column=5, row=nb_joueurs+2, sticky=(tk.W, tk.E))
mj_joueurs_perdants = ttk.Label(cadre, textvariable=j2_mj).grid(column=5, row=nb_joueurs+3, sticky=(tk.W, tk.E))


ttk_titre_mg = ttk.Label(cadre, text="nb matchs\ngagnés :").grid(column=6, row=nb_joueurs+1, sticky=tk.W)
ttk_mg_joueurs_gagnants = ttk.Label(cadre, textvariable=j1_mg).grid(column=6, row=nb_joueurs+2, sticky=(tk.W, tk.E))
ttk_mg_joueurs_perdants = ttk.Label(cadre, textvariable=j2_mg).grid(column=6, row=nb_joueurs+3, sticky=(tk.W, tk.E))

ttk_titre_mp = ttk.Label(cadre, text="nb matchs\nperdus :").grid(column=7, row=nb_joueurs+1, sticky=tk.W)
ttk_mp_joueurs_gagnants = ttk.Label(cadre, textvariable=j1_mp).grid(column=7, row=nb_joueurs+2, sticky=(tk.W, tk.E))
ttk_mp_joueurs_perdants = ttk.Label(cadre, textvariable=j2_mp).grid(column=7, row=nb_joueurs+3, sticky=(tk.W, tk.E))

ttk_titre_dt = ttk.Label(cadre, text="date du dernier\nmatch joué :").grid(column=8, row=nb_joueurs+1, sticky=tk.W)
ttk_dt_joueurs_gagnants = ttk.Label(cadre, textvariable=j1_dt).grid(column=8, row=nb_joueurs+2, sticky=tk.W)
ttk_dt_joueurs_perdants = ttk.Label(cadre, textvariable=j2_dt).grid(column=8, row=nb_joueurs+3, sticky=(tk.W, tk.E))

ttk_titre_elo = ttk.Label(cadre, text="elo :      ").grid(column=9, row=nb_joueurs+1, sticky=tk.W)
ttk_elo_joueurs_gagnants = ttk.Label(cadre, textvariable=j1_elo).grid(column=9, row=nb_joueurs+2, sticky=(tk.W, tk.E))
ttk_elo_joueurs_perdants = ttk.Label(cadre, textvariable=j2_elo).grid(column=9, row=nb_joueurs+3, sticky=(tk.W, tk.E))

ttk_titre_proba = ttk.Label(cadre, text="proba : ").grid(column=10, row=nb_joueurs+1, sticky=tk.W)
ttk_proba_joueurs_gagnants = ttk.Label(cadre, textvariable=j1_proba).grid(column=10, row=nb_joueurs+2, sticky=(tk.W, tk.E))
ttk_proba_joueurs_perdants = ttk.Label(cadre, textvariable=j2_proba).grid(column=10, row=nb_joueurs+3, sticky=(tk.W, tk.E))

ttk_titre_n_elo = ttk.Label(cadre, text="nouveau elo : ").grid(column=11, row=nb_joueurs+1, sticky=tk.W)
ttk_n_elo_joueurs_gagnants = ttk.Label(cadre, textvariable=j1_n_elo).grid(column=11, row=nb_joueurs+2, sticky=(tk.W, tk.E))
ttk_n_elo_joueurs_perdants = ttk.Label(cadre, textvariable=j2_n_elo).grid(column=11, row=nb_joueurs+3, sticky=(tk.W, tk.E))



# ___classement___

# classement joueurs
ttk_titre_classement_joueurs = ttk.Label(cadre, text="classement\n(pas fiable) :").grid(column=6, row=nb_joueurs+6, sticky=tk.E)

nom_top = [None,]*nb_joueurs
ttk_nom_joueur_top = [None,]*nb_joueurs
elo_top = [None,]*nb_joueurs
ttk_elo_joueur_top = [None,]*nb_joueurs
mj_top = [None,]*nb_joueurs
ttk_mj_joueur_top = [None,]*nb_joueurs
mg_top = [None,]*nb_joueurs
ttk_mg_joueur_top = [None,]*nb_joueurs
mp_top = [None,]*nb_joueurs
ttk_mp_joueur_top = [None,]*nb_joueurs
for n in range(nb_joueurs):
	nom_top[n] = tk.StringVar()
	nom_top[n].set(liste_nom_joueur_top[n])
	elo_top[n] = tk.StringVar()
	elo_top[n].set(joueurs[liste_nom_joueur_top[n]][0])
	mj_top[n] = tk.StringVar()
	mj_top[n].set(joueurs[liste_nom_joueur_top[n]][1])
	mg_top[n] = tk.StringVar()
	mg_top[n].set(joueurs[liste_nom_joueur_top[n]][2])
	mp_top[n] = tk.StringVar()
	mp_top[n].set(joueurs[liste_nom_joueur_top[n]][3])
	ttk_nom_joueur_top[n] = ttk.Label(cadre, textvariable=nom_top[n]).grid(column=7, row=nb_joueurs+6+n, sticky=tk.W)
	ttk_elo_joueur_top[n] = ttk.Label(cadre, textvariable=elo_top[n]).grid(column=8, row=nb_joueurs+6+n, sticky=tk.W)
	ttk_mj_joueur_top[n] = ttk.Label(cadre, textvariable=mj_top[n]).grid(column=9, row=nb_joueurs+6+n, sticky=tk.W)
	ttk_mg_joueur_top[n] = ttk.Label(cadre, textvariable=mg_top[n]).grid(column=10, row=nb_joueurs+6+n, sticky=tk.W)
	ttk_mp_joueur_top[n] = ttk.Label(cadre, textvariable=mp_top[n]).grid(column=11, row=nb_joueurs+6+n, sticky=tk.W)

# classement doubles
ttk_titre_classement_doubles = ttk.Label(cadre, text="classement\ndoubles :").grid(column=12, row=1, sticky=tk.E)

nom_double_top = [None,]*nb_doubles
ttk_nom_double_joueur_top = [None,]*nb_doubles
elo_double_top = [None,]*nb_doubles
ttk_elo_double_top = [None,]*nb_doubles
mj_double_top = [None,]*nb_doubles
ttk_mj_double_top = [None,]*nb_doubles
mg_double_top = [None,]*nb_doubles
ttk_mg_double_top = [None,]*nb_doubles
mp_double_top = [None,]*nb_doubles
ttk_mp_double_top = [None,]*nb_doubles
dt_double_top = [None,]*nb_doubles
ttk_dt_double_top = [None,]*nb_doubles
for n in range(nb_doubles): # top16
	nom_double_top[n] = tk.StringVar()
	nom_double_top[n].set(liste_nom_double_top[n])
	elo_double_top[n] = tk.StringVar()
	elo_double_top[n].set(equipes_double[liste_nom_double_top[n]][0])
	mj_double_top[n] = tk.StringVar()
	mj_double_top[n].set(equipes_double[liste_nom_double_top[n]][1])
	mg_double_top[n] = tk.StringVar()
	mg_double_top[n].set(equipes_double[liste_nom_double_top[n]][2])
	mp_double_top[n] = tk.StringVar()
	mp_double_top[n].set(equipes_double[liste_nom_double_top[n]][3])
	dt_double_top[n] = tk.StringVar()
	dt_double_top[n].set(equipes_double[liste_nom_double_top[n]][4])

	ttk_nom_double_joueur_top[n] = ttk.Label(cadre, textvariable=nom_double_top[n]).grid(column=13, row=n+1, sticky=tk.W)
	ttk_elo_double_top[n] = ttk.Label(cadre, textvariable=elo_double_top[n]).grid(column=14, row=n+1, sticky=tk.W)
	ttk_mj_double_top[n] = ttk.Label(cadre, textvariable=mj_double_top[n]).grid(column=15, row=n+1, sticky=tk.W)
	ttk_mg_double_top[n] = ttk.Label(cadre, textvariable=mg_double_top[n]).grid(column=16, row=n+1, sticky=tk.W)
	ttk_mp_double_top[n] = ttk.Label(cadre, textvariable=mp_double_top[n]).grid(column=17, row=n+1, sticky=tk.W)
	ttk_dt_double_top[n] = ttk.Label(cadre, textvariable=dt_double_top[n]).grid(column=18, row=n+1, sticky=tk.W)

#Création des zones de saisie
champs_commentaire_match = ttk.Entry(cadre, width=32, textvariable=commentaire).grid(column=3, row=nb_joueurs+4, columnspan = 3, sticky=tk.W)


# ajoute une gouttière entre les éléments
#`for enfant in cadre.winfo_children():
#    enfant.grid_configure(padx=5, pady=5)

# Emplacement initial du curseur
#k_entry.focus()

# effet de la touche [entrée]
#fenetre.bind("<Return>", calcule)

# _________________________
# _________________________
# __ 10 derniers matchs  __
# _________fenetre_________
# _________________________

fen_matchs_csv = tk.Tk()
fen_matchs_csv.geometry("1200x300")
fen_matchs_csv.resizable(width=True, height=True)
fen_matchs_csv.title("10 derniers matchs du csv")

col_names = ("ID","nom gagnant","elo gagnant","proba gagnant","nom perdant","elo perdant","proba perdant","nouveau elo gagnant","nouveau elo perdant","date","commentaire")
for i, col_name in enumerate(col_names, start=0):
    tk.Label(fen_matchs_csv, text=col_name).grid(row=0, column=i, padx=10)

with open("matchs.csv", "r", newline="") as f:
    data = list(csv.reader(f))

entrieslist = []
for i, row in enumerate(data):
    if i>(int(config["ID_dernier_match"][0]) - 11):
        entrieslist.append(row[0])
        for col in range(1, 11):
            tk.Label(fen_matchs_csv, text=row[col]).grid(row=i, column=col)

# _________________________
# _________________________
# __ Programme principal __
# _________________________
# _________________________

# Affichage et activation de la fenêtre
fenetre.mainloop()
fen_matchs_csv.mainloop()
