# framagit.org/vev
import tkinter as tk
from tkinter import ttk
import json
from csv import writer
from time import localtime, strftime
import csv
import random

"""RAF
* comment/struct prog
* seq bouton radio
* controle: si pas j1ab2ab pas bouton g/p/v: comment rendre invisible
* 😊❤😍😂🤣💖😜🐱‍🚀💋

* 5 derniers matchs
    pb index qd moins de 11 lignes
    log match cr/lf en trop
* config simple XOR double

"""

# _______________
# _______________
# * import json *
# _______________
# _______________
with open('config.json', 'r') as f:
    config = json.load(f)
with open('joueurs.json', 'r') as f:
    joueurs = json.load(f)

# liste_joueurs depuis json
liste_joueurs = list(joueurs.keys())

# ______________________________
# ______________________________
#   * prépa liste classement *
# ______________________________
# ______________________________

# ____classement_double____
classement_double ={}
for c in joueurs:     # __création dico 'classement_double' nom:elo__
    classement_double[c] = joueurs[c][0]# copie valeur dans clé
classement_double = {k: v for k, v in sorted(classement_double.items(), key=lambda item: item[1],reverse=True)} # tri décroissant par valeurs
liste_nom_double_top= list(classement_double)# extrait clés
# liste suivante pas utile car on a déjà les clés triées qu'on peut utiliser pour chercher infos dans dico
liste_elo_double_top= list(classement_double.values())# extrait valeurs

# _______________
# _______________
# ___ config ____
# _______________
# _______________

nb_joueurs = len(joueurs)
nb_doubles = len(joueurs)
ID_dernier_match = 0
var_message_bienvenue= config["message_bienvenue"][0] # message_bienvenue


# _______________
# _______________
# __ Fonctions __
# _______________
# _______________

def recup_gagnant(*args): 
	"""recup_gagnant le nom_joueurs et elo"""
	var_j1= j1.get()
	# création joueurs si pas existant
	if var_j1 not in joueurs:
		d= {var_j1:["1500", "0", "0", "0", "aucun enregistrement"],}
		joueurs.update(d)
		with open('joueurs.json', 'w') as f:
			json.dump(joueurs, f, ensure_ascii=False)
	j1.set(var_j1)
	j1_elo.set(joueurs[var_j1][0])
	j1_mj.set(joueurs[var_j1][1])
	j1_mg.set(joueurs[var_j1][2])
	j1_mp.set(joueurs[var_j1][3])
	j1_dt.set(joueurs[var_j1][4])

def recup_perdant(*args): 
	"""recup_perdant le nom_joueurs et elo
	
	         proba
	"""
	var_j1= j1.get()
	var_j2= j2.get()
	# création joueurs si pas existant
	if var_j2 not in joueurs:
		d= {var_j2:["1500", "0", "0", "0", "aucun enregistrement"],}
		joueurs.update(d)
		with open('joueurs.json', 'w') as f:
			json.dump(joueurs, f, ensure_ascii=False)
	# controle
	if var_j1 != var_j2: 
		j2.set(var_j2)
		j2_elo.set(joueurs[var_j2][0])
		j2_mj.set(joueurs[var_j2][1])
		j2_mg.set(joueurs[var_j2][2])
		j2_mp.set(joueurs[var_j2][3])
		j2_dt.set(joueurs[var_j2][4])
		# _________code1 calcul proba________________
		var_j1_elo= int(j1_elo.get())
		var_j2_elo= int(j2_elo.get())
		var_proba= 1 / (1 + 10 ** ((var_j2_elo-var_j1_elo)/400))
		j1_proba.set(round(var_proba,2))
		j2_proba.set(round(1-var_proba,2))
		# _________________________



def valider(*args): 
	"""calcule proba et nouveau elo
	maj fichiers
	"""
	# _________code1 calcul proba________________
	var_j1_elo= int(j1_elo.get())
	var_j2_elo= int(j2_elo.get())
	var_proba= 1 / (1 + 10 ** ((var_j2_elo-var_j1_elo)/400))
	j1_proba.set(round(var_proba,2))
	j2_proba.set(round(1-var_proba,2))
	# ___________calcul ELO______________
	j1_n_elo.set(round(var_j1_elo+(1-var_proba)*32))
	j2_n_elo.set(round(var_j2_elo-(1-var_proba)*32))
	# ___________double gagnant______________
	var_j1= j1.get()
	var_j1_n_elo= j1_n_elo.get()
	var_j1_mj= int(joueurs[var_j1][1]) # matchs joués
	var_j1_mg= int(joueurs[var_j1][2]) # matchs gagnés
	var_j1_mp= int(joueurs[var_j1][3]) # matchs perdus
	# ___________double perdant______________
	var_j2= j2.get()
	var_j2_n_elo= j2_n_elo.get()
	var_j2_mj= int(joueurs[var_j2][1]) # matchs joués
	var_j2_mg= int(joueurs[var_j2][2]) # matchs gagnés
	var_j2_mp= int(joueurs[var_j2][3]) # matchs perdus
	# __________MAJ JSON et CSV_______________
	dt = strftime("%Y-%m-%d %H:%M:%S", localtime()) # date/heure dernier match joué
	d= {var_j1:[str(var_j1_n_elo),str(var_j1_mj+1),str(var_j1_mg+1),str(var_j1_mp),str(dt)],var_j2:[str(var_j2_n_elo),str(var_j2_mj+1),str(var_j2_mg),str(var_j2_mp+1),str(dt)]}
	joueurs.update(d)
	with open('joueurs.json', 'w') as f:
		json.dump(joueurs, f, ensure_ascii=False)
	var_ID_dernier_match= int(config["ID_dernier_match"][0]) # numéro_dernier_match
	g= {"ID_dernier_match": [str(var_ID_dernier_match +1)]}
	config.update(g)
	with open('config.json', 'w') as f:
		json.dump(config, f, ensure_ascii=False)
	# __________RAZ var_______________
	"""j1a.set("")
	j1b.set("")
	j2a.set("")_____________ pour test
	j2b.set("")"""
	# __________log match_______________
	#ID_dernier_match = ID_dernier_match +1
	var_commentaire = commentaire.get()
	résultat_match = [str(var_ID_dernier_match +1),str(var_j1),str(var_j1_elo),str(round(var_proba,2)),str(var_j2),str(var_j2_elo),str(round(1-var_proba,2)),str(var_j1_n_elo),str(var_j2_n_elo),str(dt),str(var_commentaire)]
	with open(r'matchs simple.csv', 'a', newline='') as f:
		writer_object = writer(f)
		writer_object.writerow(résultat_match)
		f.close()



# _________________________
# _________________________
# __ Interface graphique __
# _________________________
# _________________________

# fenetre principale
fenetre = tk.Tk()
fenetre.title("elo LVGsports simple")

# élément (widget) cadre contenant tout le reste
cadre = ttk.Frame(fenetre, padding="3 3 12 12").grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E, tk.S))

# le cadre s'étire si l'on étire la fenêtre
fenetre.columnconfigure(0, weight=1)
fenetre.rowconfigure(0, weight=1)
s = ttk.Style()

print(s.theme_names())
a=random.choice(s.theme_names())
print(a)
s.theme_use(a)


# Paramètres du système (variables)
commentaire = tk.StringVar()
j1 = tk.StringVar()
j2 = tk.StringVar()
j1_elo = tk.StringVar()
j1_mj = tk.StringVar()
j1_mg = tk.StringVar()
j1_mp = tk.StringVar()
j1_dt = tk.StringVar()
j2_elo = tk.StringVar()
j2_mj = tk.StringVar()
j2_mg = tk.StringVar()
j2_mp = tk.StringVar()
j2_dt = tk.StringVar()
j1_proba = tk.IntVar()
j2_proba = tk.IntVar()
j1_n_elo = tk.IntVar()
j2_n_elo = tk.IntVar()


# Création des quatres 'boutons radio' :
for n in range(nb_joueurs):
	boutj1 = ttk.Radiobutton(cadre, text = liste_joueurs[n], variable = j1, value = liste_joueurs[n]).grid(column=1, row = n, sticky=(tk.N, tk.S, tk.W, tk.E))
	boutj2 = ttk.Radiobutton(cadre, text = liste_joueurs[n], variable = j2, value = liste_joueurs[n]).grid(column=1, row = n+nb_joueurs+5, sticky=(tk.N, tk.S, tk.W, tk.E))

# Placement des éléments (widgets)

# Création du bouton
ttk_bouton_gagne = ttk.Button(cadre, text="gagne 😊", command=recup_gagnant).grid(column=3, row=nb_joueurs+2, sticky=tk.W)
ttk_bouton_perd = ttk.Button(cadre, text="perd 😂", command=recup_perdant).grid(column=3, row=nb_joueurs+3, sticky=tk.W)
ttk_bouton_valider = ttk.Button(cadre, text="valider", command=valider).grid(column=6, row=nb_joueurs+4, sticky=tk.W)


#noms des joueurs
ttk_nom_joueurs_gagnants = ttk.Label(cadre, textvariable=j1).grid(column=4, row=nb_joueurs+2, sticky=(tk.W, tk.E))
ttk_nom_joueurs_perdants = ttk.Label(cadre, textvariable=j2).grid(column=4, row=nb_joueurs+3, sticky=(tk.W, tk.E))

# Logo et titre
tk_img = tk.PhotoImage(file = r".\v\LogonewLVGB.png")
img1 =tk_img.subsample(3, 3)
ttk_logo = ttk.Label(cadre, image = img1).grid(row = 0, column = 5, columnspan = 2, rowspan = 7, sticky=(tk.W, tk.N))
ttk_qui_quoi = ttk.Label(cadre, text=var_message_bienvenue).grid(column=7, row=3, columnspan = 2, rowspan = 2, sticky=(tk.W, tk.E))


titre_mj = ttk.Label(cadre, text="nb matchs\njoués :").grid(column=5, row=nb_joueurs+1, sticky=tk.W)
mj_joueurs_gagnants = ttk.Label(cadre, textvariable=j1_mj).grid(column=5, row=nb_joueurs+2, sticky=(tk.W, tk.E))
mj_joueurs_perdants = ttk.Label(cadre, textvariable=j2_mj).grid(column=5, row=nb_joueurs+3, sticky=(tk.W, tk.E))


ttk_titre_mg = ttk.Label(cadre, text="nb matchs\ngagnés :").grid(column=6, row=nb_joueurs+1, sticky=tk.W)
ttk_mg_joueurs_gagnants = ttk.Label(cadre, textvariable=j1_mg).grid(column=6, row=nb_joueurs+2, sticky=(tk.W, tk.E))
ttk_mg_joueurs_perdants = ttk.Label(cadre, textvariable=j2_mg).grid(column=6, row=nb_joueurs+3, sticky=(tk.W, tk.E))

ttk_titre_mp = ttk.Label(cadre, text="nb matchs\nperdus :").grid(column=7, row=nb_joueurs+1, sticky=tk.W)
ttk_mp_joueurs_gagnants = ttk.Label(cadre, textvariable=j1_mp).grid(column=7, row=nb_joueurs+2, sticky=(tk.W, tk.E))
ttk_mp_joueurs_perdants = ttk.Label(cadre, textvariable=j2_mp).grid(column=7, row=nb_joueurs+3, sticky=(tk.W, tk.E))

ttk_titre_dt = ttk.Label(cadre, text="date du dernier\nmatch joué :").grid(column=8, row=nb_joueurs+1, sticky=tk.W)
ttk_dt_joueurs_gagnants = ttk.Label(cadre, textvariable=j1_dt).grid(column=8, row=nb_joueurs+2, sticky=tk.W)
ttk_dt_joueurs_perdants = ttk.Label(cadre, textvariable=j2_dt).grid(column=8, row=nb_joueurs+3, sticky=(tk.W, tk.E))

ttk_titre_elo = ttk.Label(cadre, text="elo :      ").grid(column=9, row=nb_joueurs+1, sticky=tk.W)
ttk_elo_joueurs_gagnants = ttk.Label(cadre, textvariable=j1_elo).grid(column=9, row=nb_joueurs+2, sticky=(tk.W, tk.E))
ttk_elo_joueurs_perdants = ttk.Label(cadre, textvariable=j2_elo).grid(column=9, row=nb_joueurs+3, sticky=(tk.W, tk.E))

ttk_titre_proba = ttk.Label(cadre, text="proba : ").grid(column=10, row=nb_joueurs+1, sticky=tk.W)
ttk_proba_joueurs_gagnants = ttk.Label(cadre, textvariable=j1_proba).grid(column=10, row=nb_joueurs+2, sticky=(tk.W, tk.E))
ttk_proba_joueurs_perdants = ttk.Label(cadre, textvariable=j2_proba).grid(column=10, row=nb_joueurs+3, sticky=(tk.W, tk.E))

ttk_titre_n_elo = ttk.Label(cadre, text="nouveau elo : ").grid(column=11, row=nb_joueurs+1, sticky=tk.W)
ttk_n_elo_joueurs_gagnants = ttk.Label(cadre, textvariable=j1_n_elo).grid(column=11, row=nb_joueurs+2, sticky=(tk.W, tk.E))
ttk_n_elo_joueurs_perdants = ttk.Label(cadre, textvariable=j2_n_elo).grid(column=11, row=nb_joueurs+3, sticky=(tk.W, tk.E))



# ___classement___


# classement doubles
ttk_titre_classement_doubles = ttk.Label(cadre, text="classement :").grid(column=12, row=1, sticky=tk.E)

nom_double_top = [None,]*nb_doubles
ttk_nom_double_joueur_top = [None,]*nb_doubles
elo_double_top = [None,]*nb_doubles
ttk_elo_double_top = [None,]*nb_doubles
mj_double_top = [None,]*nb_doubles
ttk_mj_double_top = [None,]*nb_doubles
mg_double_top = [None,]*nb_doubles
ttk_mg_double_top = [None,]*nb_doubles
mp_double_top = [None,]*nb_doubles
ttk_mp_double_top = [None,]*nb_doubles
dt_double_top = [None,]*nb_doubles
ttk_dt_double_top = [None,]*nb_doubles
for n in range(nb_doubles): # top16
	nom_double_top[n] = tk.StringVar()
	nom_double_top[n].set(liste_nom_double_top[n])
	elo_double_top[n] = tk.StringVar()
	elo_double_top[n].set(joueurs[liste_nom_double_top[n]][0])
	mj_double_top[n] = tk.StringVar()
	mj_double_top[n].set(joueurs[liste_nom_double_top[n]][1])
	mg_double_top[n] = tk.StringVar()
	mg_double_top[n].set(joueurs[liste_nom_double_top[n]][2])
	mp_double_top[n] = tk.StringVar()
	mp_double_top[n].set(joueurs[liste_nom_double_top[n]][3])
	dt_double_top[n] = tk.StringVar()
	dt_double_top[n].set(joueurs[liste_nom_double_top[n]][4])

	ttk_nom_double_joueur_top[n] = ttk.Label(cadre, textvariable=nom_double_top[n]).grid(column=13, row=n+1, sticky=tk.W)
	ttk_elo_double_top[n] = ttk.Label(cadre, textvariable=elo_double_top[n]).grid(column=14, row=n+1, sticky=tk.W)
	ttk_mj_double_top[n] = ttk.Label(cadre, textvariable=mj_double_top[n]).grid(column=15, row=n+1, sticky=tk.W)
	ttk_mg_double_top[n] = ttk.Label(cadre, textvariable=mg_double_top[n]).grid(column=16, row=n+1, sticky=tk.W)
	ttk_mp_double_top[n] = ttk.Label(cadre, textvariable=mp_double_top[n]).grid(column=17, row=n+1, sticky=tk.W)
	ttk_dt_double_top[n] = ttk.Label(cadre, textvariable=dt_double_top[n]).grid(column=18, row=n+1, sticky=tk.W)

#Création des zones de saisie
champs_commentaire_match = ttk.Entry(cadre, width=32, textvariable=commentaire).grid(column=3, row=nb_joueurs+4, columnspan = 3, sticky=tk.W)


# ajoute une gouttière entre les éléments
#`for enfant in cadre.winfo_children():
#    enfant.grid_configure(padx=5, pady=5)

# Emplacement initial du curseur
#k_entry.focus()

# effet de la touche [entrée]
#fenetre.bind("<Return>", calcule)

# _________________________
# _________________________
# __ 10 derniers matchs  __
# _________fenetre_________
# _________________________

fen_matchs_csv = tk.Tk()
fen_matchs_csv.geometry("1200x300")
fen_matchs_csv.resizable(width=True, height=True)
fen_matchs_csv.title("10 derniers matchs du csv")

col_names = ("ID","nom gagnant","elo gagnant","proba gagnant","nom perdant","elo perdant","proba perdant","nouveau elo gagnant","nouveau elo perdant","date","commentaire")
for i, col_name in enumerate(col_names, start=0):
    tk.Label(fen_matchs_csv, text=col_name).grid(row=0, column=i, padx=10)

with open("matchs simple.csv", "r", newline="") as f:
    data = list(csv.reader(f))

entrieslist = []
for i, row in enumerate(data):
    if i>(int(config["ID_dernier_match"][0]) - 11):
        entrieslist.append(row[0])
        for col in range(1, 11):
            tk.Label(fen_matchs_csv, text=row[col]).grid(row=i, column=col)

# _________________________
# _________________________
# __ Programme principal __
# _________________________
# _________________________

# Affichage et activation de la fenêtre
fenetre.mainloop()
fen_matchs_csv.mainloop()
