Elo LVGsports - elo double match ranking system
==========================================================

Elo LVGsports is a libre and open source elo double match ranking system,
used for sport club like badminton, table tennis, etc.. to ensure a ranking system to players who play doubles matches. 

comment procéder
================
* Renseigner le fichier joueurs.json avec vos noms de joueurs (uniques)
* sélectionner les gagnants en haut et les perdants en bas, ex: Xavier et Martin (Xavier_Martin sera créer dans equipes_double.json dans cet ordre car cela suit l'ordre rentré dans joueurs.json)
* cliquer sur "gagnent" puis "perdent"
* mettre, si besoin, un commentaire sur le match puis cliquer sur "valider"
* le match est stocké dans "matchs.csv"
* etc.. pour chaque match

conseils
========
En attendant un fixe, il faut relancer pour voir les stats à jour

Le dossier test permet de voir une utilisation de ~40 matches joués

Le dossier v contient les fichiers .json et .csv vierges

Privilégier de le lancer via IDLE pour que les erreurs ne fassent pas planter le programme


url:
====
The Elo LVGsports gitlab website  . . . . . https://framagit.org/vev/elo-lvgsports